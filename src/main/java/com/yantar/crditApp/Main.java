package com.yantar.crditApp;

import com.yantar.crditApp.client.ConsoleReader;
import com.yantar.crditApp.core.Person;

public class Main {

    public static void main(String[] args) {
        Person person = new ConsoleReader().readInputParameters();

        System.out.println("Hello, " + person.getName() + " " + person.getLastName() + ".");
    }
}
