package com.yantar.crditApp.client;

import com.sun.source.doctree.SystemPropertyTree;
import com.yantar.crditApp.core.Person;

import java.util.Scanner;

public class ConsoleReader {

    public Person readInputParameters() {
        Scanner in = new Scanner(System.in);

        type("Enter your name:");
        String name = in.next();

        type("Enter your lastname:");
        String lastName = in.next();

        type("Enter your mothers maiden name:");
        String mothersMaidenName = in.next();

        return new Person(name, lastName, mothersMaidenName);
    }

    private void type(String s) {
        System.out.println(s);
    }
}
